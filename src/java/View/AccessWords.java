/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import Controller.ConnectionManager;
import Model.UserAccess;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 *
 * @author KNV
 */
public class AccessWords {

    public static void main(String[] args){
        ConnectionManager cm = new ConnectionManager();
        UserAccess ua = new UserAccess();
        Connection con = cm.getConnection();
        Statement stmt ;
        try{
            stmt = con.createStatement();
            String sql = "SELECT * FROM easy_word ORDER BY id ASC";
            ResultSet rs = stmt.executeQuery(sql);
            System.out.println("\nID\tWORD\t\tHINT");
            while ( rs.next() ) {
                int id = rs.getInt("id");
                String  word = rs.getString("word");
                String hint  = rs.getString("hint");
                System.out.println(id+"\t"+word+"\t\t"+hint);
            }
            rs.close();
            stmt.close();
            con.close();
        } catch(Exception e){
            System.out.println("Exception "+e);
        }
    }
}
