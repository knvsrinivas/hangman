/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.LinkedList;

/**
 *
 * @author KNV
 */
public class LeaderAccess {
    private LinkedList<String> name = new LinkedList<String>();
    private LinkedList<Integer> score = new LinkedList<Integer>();
    private LinkedList<String> time = new LinkedList<String>();

    
    public void setName(String name){
        this.name.add(name);
    }
    
    public LinkedList<String> getNames(){
        return this.name;
    }
    
    public void setScore(int score){
        this.score.add(score);
    }
    
    public LinkedList<Integer> getScores(){
        return this.score;
    }
    
    public void setTime(String time){
        this.time.add(time);
    }
    
    public LinkedList<String> getTimes(){
        return this.time;
    }
    
}
