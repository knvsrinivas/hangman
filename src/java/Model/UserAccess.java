/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;
/**
 *
 * @author KNV
 */
public class UserAccess {
    private String userId;
    private String name;
    private float score;
    private String hint;
    private String guessWord;
    private int limitCount;
    private String difficulty;
    private float time;
    public UserAccess(){
        userId="";
        name="";
        score=-1;
        hint="";
        guessWord="";
        limitCount=9;
        difficulty="";
        time=-1;
    }

    
    public void setWordHint(String hint){
        this.hint = hint;
    }
    
    public String getWordHint(){
        return this.hint;
    }
    
    public void setTime(float time){
        this.time = time;
    }
    
    public float getTime(){
        return this.time;
    }
    
    public void setDifficulty(String difficulty){
        this.difficulty = difficulty;
    }
    
    public String getDifficulty(){
        return this.difficulty;
    }
    
    public void setLimitCount(int limitCount){
        this.limitCount = limitCount;
    }
    
    public int getLimitCount(){
        return this.limitCount;
    }
    
    public void setGuessWord(String guessWord){
        this.guessWord = guessWord;
    }
    
    public String getGuessWord(){
        return this.guessWord;
    }
    
    public void setName(String name){
        this.name = name;
    }
    
    public String getName(){
        return this.name;
    }
    
    public void setUserId(String userId){
        this.userId = userId;
    }
    
    public String getUserId(){
        return this.userId;
    }
    
    public void setScore(float score){
        this.score = score;
    }
    
    public float getScore(){
        return this.score;
    }
}
