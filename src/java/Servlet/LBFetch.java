/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import Controller.LBRetrieve;
import Model.LeaderAccess;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author KNV
 */
public class LBFetch extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if(request.getParameter("status").equals("getleaderboard")){
            processRequest(request, response);
            Gson gson = new Gson(); 
            JsonObject myObj = new JsonObject();
            PrintWriter out = response.getWriter();
            response.setContentType("text/html");
            response.setHeader("Cache-control", "no-cache, no-store");
            response.setHeader("Pragma", "no-cache");
            response.setHeader("Expires", "-1");
            LeaderAccess lb = new LeaderAccess();
            lb=LBRetrieve.Access(lb);
            JsonElement leaderObj = gson.toJsonTree(lb);
            if( lb.getNames().size() <1 ){
                myObj.addProperty("success", false);            
            }
            else {
                myObj.addProperty("success", true);      
            }
            /*
            LinkedList<String> name = new LinkedList<String>();
            LinkedList<Integer> score = new LinkedList<Integer>();
            LinkedList<String> time = new LinkedList<String>();
            name = lb.getNames();
            score = lb.getScores();
            time = lb.getTimes();
            */
            myObj.add("leaderboard", leaderObj);
            out.println(myObj.toString());
        } else {
            System.out.println("Exit");
            System.exit(0);
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
