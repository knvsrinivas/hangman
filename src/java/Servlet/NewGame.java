/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import Controller.GenerateWord;
import Model.UserAccess;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author KNV
 */
public class NewGame extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
         try {
             if(request.getParameter("difficulty").equals("easy")||request.getParameter("difficulty").equals("medium")||request.getParameter("difficulty").equals("hard")){
                 UserAccess ua = new UserAccess();
                 ua.setUserId(request.getParameter("user_id"));
                 ua.setDifficulty(request.getParameter("difficulty"));
                 ua.setLimitCount(10);
                 GenerateWord gw= new GenerateWord();
                 ua=gw.getWord(ua);
                 PrintWriter out = response.getWriter();
                 response.setContentType("text/html");
                 response.setHeader("Cache-control", "no-cache, no-store");
                 response.setHeader("Pragma", "no-cache");
                 response.setHeader("Expires", "-1");
                 Cookie ck_userid=new Cookie("UserID",ua.getUserId());
                 ck_userid.setMaxAge(60*5);
                 Cookie ck_word=new Cookie("Word",ua.getGuessWord().toUpperCase());
                 ck_word.setMaxAge(60*5);
                 Cookie ck_hint=new Cookie("Hint",ua.getWordHint().toUpperCase());
                 ck_hint.setMaxAge(60*5);
                 response.addCookie(ck_userid);response.addCookie(ck_hint);response.addCookie(ck_word);
                 response.sendRedirect("game.jsp");
             } else {
                response.sendRedirect("index.jsp");
             }
            
        } catch(Exception e){}
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
       
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
