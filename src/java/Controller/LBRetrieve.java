/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.LinkedList;
import Model.LeaderAccess;
import Model.UserAccess;

/**
 *
 * @author KNV
 */
public class LBRetrieve {
    static Connection currentCon = null;
    static ResultSet rs = null;
    static LeaderAccess lb;
    static PreparedStatement ps=null;
    public static boolean leaderInsert(UserAccess obj){
       Statement stmt = null;    
      try 
      {
         currentCon = ConnectionManager.getConnection();
         stmt=currentCon.createStatement();
         ps=currentCon.prepareStatement("INSERT INTO leader_board(unique_id,name,score,time) VALUES(?,?,?,?)");
         ps.setString(1,obj.getUserId());
         ps.setString(2,obj.getName());
         ps.setFloat(3,Math.round(obj.getScore()));
         ps.setFloat(4,obj.getTime());
         ps.execute();
     
         stmt.close();
         currentCon.close();
         return true;
      } 
      catch (Exception ex) 
      {
         System.out.println("Insert Failed! " + ex);
         return false;
      } 
      
    }
    public static LeaderAccess Access(LeaderAccess obj) {
      Statement stmt = null;    
      try 
      {
         currentCon = ConnectionManager.getConnection();
         stmt=currentCon.createStatement();
         ps=currentCon.prepareStatement("SELECT * FROM leader_board ORDER BY SCORE DESC LIMIT 10");
         rs= ps.executeQuery();
         
         while(rs.next()){
            String name=rs.getString(3);
            Integer score=rs.getInt(4);
            String time=rs.getString(5);
            obj.setName(name);
            obj.setScore(score);
            obj.setTime(time);
         }
         
         stmt.close();
         currentCon.close();
      } 
      catch (Exception ex) 
      {
         System.out.println("Select Failed! " + ex);
      } 
    return obj;
    }	
}
