/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;
/**
 *
 * @author KNV
 */

import java.sql.Connection;
import java.sql.DriverManager;

public class ConnectionManager {
    static Connection con = null;
    static String url;    
    public static Connection getConnection() {    
        try {
            Class.forName("org.postgresql.Driver");
            url = "jdbc:postgresql://localhost:5432/test_db";
            //database name: test_db
            con = DriverManager.getConnection(url,"postgres", "1234");
            //username : postgres  ,  Password: 1234
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println(e.getClass().getName()+": "+e.getMessage());
            System.exit(0);
        }
        System.out.println("Connected database successfully");
        return con;
    }   
}