/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.UserAccess;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 *
 * @author KNV
 */
public class GenerateWord {
    Connection currentCon = null;
    ResultSet rs = null;
    UserAccess user;
    PreparedStatement ps=null;
    public  UserAccess getWord(UserAccess obj) {
        Statement stmt = null;    
        try 
        {
            currentCon = ConnectionManager.getConnection();
            stmt=currentCon.createStatement();
            if(obj.getDifficulty().equals("easy")){
                ps=currentCon.prepareStatement("SELECT * FROM easy_word ORDER BY RANDOM() LIMIT 1");
            } else if(obj.getDifficulty().equals("medium")){
                ps=currentCon.prepareStatement("SELECT * FROM medium_word ORDER BY RANDOM() LIMIT 1");
            } else if(obj.getDifficulty().equals("hard")){
                ps=currentCon.prepareStatement("SELECT * FROM hard_word ORDER BY RANDOM() LIMIT 1");
            }
            rs= ps.executeQuery();
            String word="";
            String hint="";
            while(rs.next()){
                word=rs.getString(2);
                hint=rs.getString(3);
            }
            obj.setGuessWord(word);
            obj.setWordHint(hint);
            stmt.close();
            currentCon.close();
        }  
        catch (Exception ex) 
        {
            System.out.println("Select Failed! " + ex);
        } 
        return obj;
    }
}
