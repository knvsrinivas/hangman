--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.5
-- Dumped by pg_dump version 9.6.5

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: easy_word; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE easy_word (
    id integer NOT NULL,
    word character varying(15),
    hint character varying(20)
);


ALTER TABLE easy_word OWNER TO postgres;

--
-- Name: easy_word_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE easy_word_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE easy_word_id_seq OWNER TO postgres;

--
-- Name: easy_word_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE easy_word_id_seq OWNED BY easy_word.id;


--
-- Name: hard_word; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE hard_word (
    id integer NOT NULL,
    word character varying(15),
    hint character varying(20)
);


ALTER TABLE hard_word OWNER TO postgres;

--
-- Name: hard_word_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE hard_word_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE hard_word_id_seq OWNER TO postgres;

--
-- Name: hard_word_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE hard_word_id_seq OWNED BY hard_word.id;


--
-- Name: leader_board; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE leader_board (
    id integer NOT NULL,
    unique_id character varying(12) NOT NULL,
    name character varying(20),
    score integer,
    "time" text
);


ALTER TABLE leader_board OWNER TO postgres;

--
-- Name: leader_board_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE leader_board_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE leader_board_id_seq OWNER TO postgres;

--
-- Name: leader_board_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE leader_board_id_seq OWNED BY leader_board.id;


--
-- Name: medium_word; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE medium_word (
    id integer NOT NULL,
    word character varying(15),
    hint character varying(20)
);


ALTER TABLE medium_word OWNER TO postgres;

--
-- Name: medium_word_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE medium_word_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE medium_word_id_seq OWNER TO postgres;

--
-- Name: medium_word_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE medium_word_id_seq OWNED BY medium_word.id;


--
-- Name: easy_word id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY easy_word ALTER COLUMN id SET DEFAULT nextval('easy_word_id_seq'::regclass);


--
-- Name: hard_word id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY hard_word ALTER COLUMN id SET DEFAULT nextval('hard_word_id_seq'::regclass);


--
-- Name: leader_board id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY leader_board ALTER COLUMN id SET DEFAULT nextval('leader_board_id_seq'::regclass);


--
-- Name: medium_word id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY medium_word ALTER COLUMN id SET DEFAULT nextval('medium_word_id_seq'::regclass);


--
-- Data for Name: easy_word; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY easy_word (id, word, hint) FROM stdin;
2	hot dog	food
3	motorola	company
4	zoho	company
5	mathematics	subject
6	chemistry	subject
7	physics	subject
8	chennai	places
9	visakhapatnam	places
10	trichy	places
11	game of thrones	tv shows
12	suits	tv shows
13	friends	tv shows
1	LAPTOP	electronics
\.


--
-- Name: easy_word_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('easy_word_id_seq', 13, true);


--
-- Data for Name: hard_word; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY hard_word (id, word, hint) FROM stdin;
1	LEAPORD	ANIMALS
2	LEMMING	ANIMALS
3	ISTANBUL	PLACES
4	TURKEY	PLACES
5	TAJ MAHAL	MONUMENT
\.


--
-- Name: hard_word_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('hard_word_id_seq', 5, true);


--
-- Data for Name: leader_board; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY leader_board (id, unique_id, name, score, "time") FROM stdin;
1	HM912345789	KNV Srinivas	1500	100
2	HM967456789	knv	1400	110
3	HM962344789	Srinivas	1300	120
4	HM962654345	K Nachiappan	1200	130
6	HM962655466	Alan Francis	1600	90
7	HM9555555555	Nachiappan	1100	140
8	HM8248899189	a	13599	7.9369998
9	HM9026024147	knv	6896	16.9619999
10	HM2226236863	Alan	17160	6.1880002
11	HM8948904239	Nachiappan	14401	7.46199989
12	HM2721584476	knv worng	0	21.1639996
13	HM1195512047	yo failed	0	27.3570004
14	HM4151041468	srinivas	1043	23.0130005
15	HM7799569154	knv	1129	7.73600006
16	HM5812213884	Srinivas	7711	12.9680004
17	HM6851339925	Alan Francis	1088	11.3920002
18	HM4447206927	Nachi	1062	16.2530003
19	HM2000142706	Nachi	1077	12.9720001
\.


--
-- Name: leader_board_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('leader_board_id_seq', 19, true);


--
-- Data for Name: medium_word; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY medium_word (id, word, hint) FROM stdin;
1	JAZZ	MUSICAL INSTRUMENT
2	EIFFEL TOWER	MONUMENT
3	FOX	ANIMAL
4	CHEETAH	ANIMAL
5	RABBIT	ANIMAL
6	DELHI	PLACES
7	CALIFORNIA	PLACES
\.


--
-- Name: medium_word_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('medium_word_id_seq', 7, true);


--
-- Name: leader_board leader_board_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY leader_board
    ADD CONSTRAINT leader_board_pkey PRIMARY KEY (unique_id);


--
-- PostgreSQL database dump complete
--

