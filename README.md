# Hangman
Database Name: test_db<br>
Database Username : postgres<br>
Database Password : 1234<br>
<hr>
Use the file test_dbBackUp to import the database into your postgresql using pgAdmin 4.<br>
Add login for the database as above.<br>
Or change the username and password for the database in src/java/Controller/ConnectionManager.java file to required username and password.<br>
Only Leaderboard has been completed.<br>
The remaining part is yet to be completed.<br>
The leaderboard database is prepopulated.<br>
The tables in test_db are : 1)leader_board  2)word_list<br>
Leader_board contains the leaderboard details.<br>
Word_list contains words for the game.<br>

<hr>
<h1>PostgreSQL Database Restore</h1>
create a database with name test_db in postgresql using pgAdmin.<br>
go to the source folder where postgres is installed <br>
in windows  <b>C:\Program Files\PostgreSQL\9.6\bin</b><br>
open command prompt here<br>
copy the backup.sql into this directory<br>
execute the below command to restore the databse.<br>
psql -U <username> -d test_db -1 -f backup.sql<br>
here <username> is the Database Username (postgres).<br>
