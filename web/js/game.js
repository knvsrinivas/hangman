/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) === ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) === 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

if(getCookie("UserID")!==""){
    var usrid=getCookie("UserID");
    var word=getCookie("Word");
    var hint=getCookie("Hint");
    if(word.charAt(0)==="\""){
        word=word.substr(1,word.length-2);
    }
    if(hint.charAt(0)==="\""){
        hint=hint.substr(1,hint.length-2);
    }
    console.log(word);
    document.getElementById('user_id').setAttribute("name","user_id");
    document.getElementById('user_id').setAttribute("value",usrid);
    initGame(word,hint);
}else {
    window.location.href = "index.jsp";
}


function initGame(word,hint){
    var hint_final=hint.toUpperCase();
    word=word.toUpperCase();
    document.getElementById("hint").innerHTML=hint_final;
    
    var len=word.length;
    var box="<div class=\"word_box\" id=pos-";
    var tail="></div>";
    var blank="<div class=\"word_box hide\" id=pos-";
    var str="ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    for (var i = 0; i <len; i++) {
        if(word.charAt(i)!==" ")
            $('.word').append(box+i+tail);
        else
            $('.word').append(blank+i+tail);
       
    }
    var alpha_box="<button style=\"cursor:pointer;\" class=\"alpha_box\" id=";
    var alpha_tail="</button>";
    for(var j=0;j<26;j++){
        $('.input_box').append(alpha_box+str.charAt(j)+">"+str.charAt(j)+alpha_tail);    
    }
    localStorage.setItem("chance_count", 9);
    localStorage.setItem("guess_word", word);
    localStorage.setItem("length",word.replace(/\s/g, '').length);
    localStorage.setItem("start_time",Date.now("hh:mm:ss"));
    localStorage.setItem("img","hanged");
}

function endGame(){
    var str="ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    for(var i=0;i<26;i++){
        document.getElementById(str.charAt(i)).disabled = true;
        $("#"+str.charAt(i)).css("cursor", "no-drop");
                
    }
    if(localStorage.getItem("length")==="0"){
        var time=localStorage.getItem("start_time");
        var score=1000/time+1000;
        if(score<100){
            score=50+time*0.9;
        }
        localStorage.setItem("score",score);
        alert("You Won\nTime: "+time);
    } else{
        var time=localStorage.getItem("start_time");
        var diff=localStorage.getItem("length");
        var score=100000/(time+diff*10);
        if(score<100){
            score=50+time*0.9;
        }
        localStorage.setItem("score",score);    
        //alert(score+"   "+time+"  "+diff);
        console.log(diff);
        var word=localStorage.getItem("guess_word");
        var len=word.replace(/\s/g, '').length;
        for(var i=0;i<len;i++){
            var p="pos-";
            p=p+i;
            document.getElementById(p).innerHTML=word.charAt(i);
        }
    }
    alert(localStorage.getItem("score"));
    delete_cookie('UserID');
    delete_cookie('Word');
    delete_cookie('Hint');
    openLB_CH();
}
$("#submit").submit(function(e){
        e.preventDefault();   
    
});    
var delete_cookie = function(name) {
    document.cookie = name + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
};
$("#submit").click(function(e){
    var status="insertuser";
    var name=$("#name").val();
    var time=localStorage.getItem("start_time");
    var score=localStorage.getItem("score");
    var id=$("#user_id").attr("value");
    dataString = "status=" +status+"&id="+id+"&score="+score+"&time="+time+"&name="+name;
    $.ajax({
        type: "POST",
        url: "LBInsert",
        data: dataString,
        dataType: "text",
        
        success: function( data, textStatus, reqXHR) {         
            //alert(data);
            closeLB_CH();
            localStorage.clear();
            delete_cookie('UserID');
        },
           
        error: function(reqXHR, textStatus, errorThrown){
            console.log("Error :" + textStatus);
            $("#error").html(reqXHR.responseText);      
        },
           
        beforeSend: function(reqXHR, settings){
            $('#submit').attr("disabled", true);
        },
         
        complete: function(jqXHR, textStatus){
            $('#submit').attr("disabled", false);
            window.setTimeout(function(){ window.location.href = "index.jsp";},1000);
            localStorage.clear();
            delete_cookie('UserID');
        }
        });
});

$(document.body).click(function(evt){
  var clicked = evt.target;
  var currentID = clicked.id;
  str="ABCDEFGHIJKLMNOPQRSTUVWXYZ";
  var len=localStorage.getItem("length");
  if(len>0){
    if(str.includes(currentID)){
        var word=localStorage.getItem("guess_word");
        var count=localStorage.getItem("chance_count");
        if(word.includes(currentID)){
            var p="pos-";
            for(var i=0;i<word.length;i++){
                if(word.charAt(i)===currentID){
                    document.getElementById(p+i).innerHTML=word.charAt(i);
                    document.getElementById(currentID).disabled = true;
                    var id="#"+currentID;
                    $(id).css("cursor", "no-drop");
                    $(id).addClass("strikediag");
                    len--;
                    localStorage.setItem("length",len);
                    if(len===0){
                        var now=Date.now("hh:mm:ss");
                        var diff=now-localStorage.getItem("start_time");
                        localStorage.setItem("start_time",diff/1000);
                        console.log("WON");
                        endGame();
                    }
                }
            }
        } else{
            if(count>0){
                var id="#"+currentID;
                $(id).css("cursor", "no-drop");
                $(id).addClass("strikediag");
                var id_img="#hang_img";
                var img=localStorage.getItem("img");
                var z=10-count;
                count--;
                $(id_img).removeClass(img);
                img=img.substr(0,6)+z;
                $(id_img).addClass(img);
                localStorage.setItem("img",img);
                document.getElementById(currentID).disabled = true;
                localStorage.setItem("chance_count",count);
                if(count===0){
                    var tim=Date.now("hh:mm:ss");
                    var diff=tim-localStorage.getItem("start_time");
                    localStorage.setItem("start_time",diff/1000);
                    setTimeout(endGame() , 1000);
                }
            } 
        } 
    }
  } 
});