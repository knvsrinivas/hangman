<%-- 
    Document   : game
    Created on : Nov 10, 2017, 9:56:56 AM
    Author     : KNV
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Hangman Game</title>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet">
        <script src="js/jquery-3.2.1.min.js"></script>
    </head>
    <body>
        <div id="choice" class="overlay_choice">
            <div class="overlay-content_ch">
                <input id="name" type="text" class="name" name="name" placeholder="Enter Your Name" value=""/><br><br>
                <button class="btn btn-normal btn-normal1 pos_center" id="submit"  name="status" value="insert" >Submit</button>
            </div>
        </div>
        <div class="header">
            <h1 id="left">HANGMAN</h1>
        </div>
        <div class="row pad-bot">
            <div class="col-md-12">
                <div class="hanged center-img" id="hang_img"  alt="" ></div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="word">                
                </div>
                <div id="hint" class="hint"></div>
                <div class="input_box"></div>
            </div>
        </div>
        <div id="error"></div>
        <input type="text" class="hidden" id="user_id"/>
    </body>
    <script src="js/game.js"></script>
    
    <script src="js/id.js"></script>
</html>
