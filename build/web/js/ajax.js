/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function() {
    $("#leader").submit(function(e){
        e.preventDefault();   
    
    });    
    
    $("#leader").click(function(e){
        var st=document.getElementById("dummy").innerHTML;
        if(st==="1"){
            openLB();
            e.preventDefault();
        } else {
        var status="getleaderboard";
        dataString = "status=" +status;
        $.ajax({
            type: "POST",
            url: "LBFetch",
            data: dataString,
            dataType: "json",
            
            success: function( data, textStatus, reqXHR) {         
                //console.log("data : "+data);
                var length = data.leaderboard.name.length;
                //console.log("Length is  : "+data.leaderboard.name+"   " + data.leaderboard.score+"   "+data.leaderboard.time);
                for(var i=0;i<length;i++){
                    $("<tr id=lb"+i+"><td>"+(i+1)+"</td><td>"+data.leaderboard.name[i]+"</td><td>"+data.leaderboard.score[i]+"</td><td>"+data.leaderboard.time[i]+"</td></tr>").insertBefore("#dummy");
                }
                document.getElementById("dummy").innerHTML+=1;
                openLB();
            },
               
            error: function(reqXHR, textStatus, errorThrown){
                console.log("Error :" + textStatus);
                $("#error").html(reqXHR.responseText);      
            },
               
            beforeSend: function(reqXHR, settings){
                $('#leader').attr("disabled", true);
                $("#new_game").attr("disabled",true);
            },
          
            complete: function(jqXHR, textStatus){
                $('#leader').attr("disabled", false);
                $("#new_game").attr("disabled",false);
            }
        });        
    }
    }); 
    
});
