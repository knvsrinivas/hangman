<%--
    Document   : index
    Created on : Oct 24, 2017, 9:57:06 PM
    Author     : KNV
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Hangman Game</title>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet">
        <script src="js/jquery-3.2.1.min.js"></script>

    </head>
    <body>
        <h1 id="pos_center">Hangman</h1>
         <!-- Template Buttons
         <button class="btn btn-circle btn-circle1"  >Yes</button>
         <button class="btn btn-normal btn-normal1" >Yes</button>
         -->
         <form  id="game" method="POST" action="NewGame" >           
            <div class="center_main margin_top" style="width:100%;">
               <!-- <button class="btn btn-normal btn-normal1 pos_center" id="new_game" name="status" value="newgame" onclick="form.action='NewGame';">New Game</button>
                -->
              <button class="btn btn-normal btn-normal1 pos_center" id="new_game" type="button"  onclick="openLB_CH()" >New Game</button>
            </div>
            <div id="choice" class="overlay_choice">
                <a href="javascript:void(0)" class="closebtn_choice" onclick="closeLB_CH()">&times;</a>
                <div class="overlay-content_ch">
                    <button class="btn btn-normal btn-normal1 pos_center" id="easy"  name="difficulty" value="easy" >Easy</button>
                    <button class="btn btn-normal btn-normal1 pos_center" id="medium"  name="difficulty" value="medium" >Medium</button>
                    <button class="btn btn-normal btn-normal1 pos_center" id="hard"  name="difficulty" value="hard" >Hard</button>
                </div>
            </div>
            <div class="center_main" style="width:100%;" >
                <button class="btn btn-normal btn-normal1 pos_center" id="leader" name="status" value="getleaderboard">Leader Board</button>
            </div>
            <input type="text" class="hidden" id="user_id"/>
         </form>
         <!--- TEST Overlay
         <div class="center_main">
                <button class="btn btn-normal btn-normal1 pos_center" id="leader"  onclick="openLB()">Leader Board</button>
            </div>
         --->
         <div id="lb" class="overlay">
            <a href="javascript:void(0)" class="closebtn" onclick="closeLB()">&times;</a>
            <div class="overlay-content">
                <table id="leader_table" class="lbtable">
                    <tr id="lbhead">
                        <th id="sno">Rank</th><th id="name">Name</th><th id="score">Score</th><th id="time">Time(s)</th>
                    </tr>                    
                    <tr class="hidden" id="dummy"></tr>
                </table>
            </div>
         </div>
         
         <div id="error"></div>
    </body>
    <script>
    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for(var i = 0; i <ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) === ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) === 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }
    if(getCookie("UserID")!==""){
        window.location.href="game.jsp";
    }
    </script>
    <script src="js/id.js"></script>
    <script src="js/ajax.js"></script>
    <script src="js/jquery.min.js"></script>
   
</html>
